/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.plantillam15pt22controlversionsadn;

import com.mycompany.plantillam15pt22controlversionsadn.libraries.ADNFileReader;
import static java.lang.Math.round;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author alejandro i gerard
 */
public class ProgramaADN {

    public static void main(String[] args) {

        // 1. Mètode llegir fitxer amb una cadena ADN.
        String rutaFitxerADN = "";
        String ADNFitxer = ADNFileReader.llegeixFitxerADN("dnaSequence.txt");

        // 2. Validar l'ADN del fitxer
        boolean validatedDNA = ADNFileReader.validateDNASeq(ADNFitxer);
        System.out.println(validatedDNA);
        // 3a. Si és vàlid, ja podem mostrar el menu
        if (!validatedDNA) {
            Scanner userInput = new Scanner(System.in);
            boolean activeMenu = true;
            int userOption = 0;
            do {
                System.out.println(" === MENU === ");
                System.out.println("1. Donar la volta a l’ADN. (retorn String)\n"
                        + "2. Trobar la base més repetida. (retorn caràcter)\n"
                        + "3. Trobar la base menys repetida. (retorn caràcter)\n"
                        + "4. Fer recompte de bases i mostrar-lo. (1*)\n"
                        + "5. Convertir l’ADN a ARN (2*)\n"
                        + "6. Número de vegades que apareix un tros de cadena ADN. (3*)\n"
                        + "7. Sortir.");
                System.out.print("Introduce your option> ");
                try {
                    userOption = userInput.nextInt();
                } catch (java.util.InputMismatchException E) {
                    System.out.println(E);
                }
                switch (userOption) {
                    case 1:
                        String DNAInvert = ADNFileReader.invertDNA(ADNFitxer);
                        System.out.println("The original sequence: " + ADNFitxer + "\nThe invert sequence is:" + DNAInvert);
                        break;
                    case 2:
                        char mostRepeatedNuc = ADNFileReader.findMostRepeatedNucleotide(ADNFitxer);
                        System.out.println("The most repeated nucleotide: " + mostRepeatedNuc);
                        break;
                    case 3:
                        char leastRepeatedNuc = ADNFileReader.findLeastRepeatedNucleotide(ADNFitxer);
                        System.out.println("The least repeated nucleotide: " + leastRepeatedNuc);
                        break;
                    case 4:
                        ADNFileReader.recompNuc(ADNFitxer);
                        break;
                    case 5:
                        ADNFileReader.dnaToRna(ADNFitxer);
                        break;
                    case 6:
                        ADNFileReader.repeatedCodons(ADNFitxer);
                        break;
                    case 7:
                        activeMenu = false;
                        System.out.println("/// CLOSING PROGRAM ///");
                        break;
                    default:
                        System.out.println("/// Introudce a correct option ///");
                        break;
                }
            } while (activeMenu);

        }
        // 3b. Sortim del programa si l'ADN no es valid.
    }
}
