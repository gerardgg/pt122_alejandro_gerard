/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.plantillam15pt22controlversionsadn.libraries;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;

/**
 *
 * @author alejandro i gerard
 */
public class TextFileReaderDemo {

    public static String filreReader() {
        try {
            FileInputStream fstream = new FileInputStream("dnaSequence.txt");
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            String strFile = "";

            while ((strLine = br.readLine()) != null) {
                strFile += strLine;
            }

            in.close();
            
            return strFile;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }
}