package com.mycompany.plantillam15pt22controlversionsadn.libraries;

/**
 * Llibreria que conté mètodes per a tractar cadenes d'ADN.
 * @author alejandro i gerard
 */
public class ManagerADN {
    
//- Donar la volta a l’ADN. (retorn String)
    
    /**
     * Retorna la cadena ADN passada per paràmtre invertida.
     * @param ADN Cadena ADN.
     * @return Cadena ADN Invertida
     */
    public static String donarVoltaADN(String ADN) {
        return "";
    }
    
    /**
     * 
     * @param ADN
     * @return True si es correcte, False si no es un ADN valid.
     */
    public static boolean EsCorrecteADN(String ADN) {
        
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        return false;
    }
    
    
//- Trobar la base més repetida. (retorn caràcter)
//- Trobar la base menys repetida. (retorn caràcter)
//- Fer recompte de bases i mostrar-lo. (1*)
//- Convertir l’ADN a ARN (2*)
//- Número de vegades que apareix un tros de cadena ADN. (3*)
    
//    https://gitlab.com/dawbio2-m15-uf1-a01-2020/provant-exp-regulars/-/blob/master/ProvaExpRegulars/src/provaexpregulars/ExpRegularsADN.java
//    // Contar cuantas veces aparece en un ADN la cadena "cat" o "gat"
//       ADNcadena = "catagacatcat";
//       System.out.println("cat cuantas veces aparece en el ADN catagacatcat? "); 
//       pat = Pattern.compile("cat");
//       mat = pat.matcher(ADNcadena);
//       int cont = 0;
//       while (mat.find()) {
//           cont++;
//       }

    
    
//- Sortir.

}
