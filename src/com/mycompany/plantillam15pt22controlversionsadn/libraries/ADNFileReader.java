package com.mycompany.plantillam15pt22controlversionsadn.libraries;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import static java.lang.Math.round;

/**
 * 
 * @author alejandro i gerard
 */
public class ADNFileReader {
    
      public static String llegeixFitxerADN(String nomFitxer) {
            try {
            FileInputStream fstream = new FileInputStream(nomFitxer);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            String strFile = "";

            while ((strLine = br.readLine()) != null) {
                strFile += strLine;
            }

            in.close();
            
            return strFile;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
      }
      
      public static boolean validateDNASeq(String DNASeq) {
        boolean errorFlag = false;
        char[] nucleotides = {'A', 'C', 'T', 'G'};
        for (int i = 0; i < DNASeq.length(); i++) {
            if (!errorFlag) {
                for (char nucleotide : nucleotides) {
                    if (DNASeq.charAt(i) == nucleotide) {
                        errorFlag = false;
                        break;
                    } else {
                        errorFlag = true;
                    }
                }
            } else {
                break;
            }
        }
        return errorFlag;
    }

    public static String invertDNA(String DNASeq) {
        String DNAInvert = "";
        for (int DNALength = DNASeq.length() - 1; DNALength >= 0; DNALength--) {
            DNAInvert += DNASeq.charAt(DNALength);
        }
        return DNAInvert;

    }

    public static char findLeastRepeatedNucleotide(String DNASeq) {
        char leastRepeatedNuc = 0;
        int aOcurrencies = countA(DNASeq);
        int cOcurrencies = countC(DNASeq);
        int tOcurrencies = countT(DNASeq);
        int gOcurrencies = countG(DNASeq);

        if (aOcurrencies < cOcurrencies && aOcurrencies < tOcurrencies && aOcurrencies < gOcurrencies) {
            leastRepeatedNuc = 'A';
        } else if (cOcurrencies < aOcurrencies && cOcurrencies < tOcurrencies && cOcurrencies < gOcurrencies) {
            leastRepeatedNuc = 'C';
        } else if (tOcurrencies < aOcurrencies && tOcurrencies < cOcurrencies && tOcurrencies < gOcurrencies) {
            leastRepeatedNuc = 'T';
        } else if (gOcurrencies < aOcurrencies && gOcurrencies < tOcurrencies && gOcurrencies < cOcurrencies) {
            leastRepeatedNuc = 'G';
        }
        return leastRepeatedNuc;
    }

    public static char findMostRepeatedNucleotide(String DNASeq) {
        char mostRepeatedNuc = 0;
        int aOcurrencies = countA(DNASeq);
        int cOcurrencies = countC(DNASeq);
        int tOcurrencies = countT(DNASeq);
        int gOcurrencies = countG(DNASeq);

        if (aOcurrencies > cOcurrencies && aOcurrencies > tOcurrencies && aOcurrencies > gOcurrencies) {
            mostRepeatedNuc = 'A';
        } else if (cOcurrencies > aOcurrencies && cOcurrencies > tOcurrencies && cOcurrencies > gOcurrencies) {
            mostRepeatedNuc = 'C';
        } else if (tOcurrencies > aOcurrencies && tOcurrencies > cOcurrencies && tOcurrencies > gOcurrencies) {
            mostRepeatedNuc = 'T';
        } else if (gOcurrencies > aOcurrencies && gOcurrencies > tOcurrencies && gOcurrencies > cOcurrencies) {
            mostRepeatedNuc = 'G';
        }
        return mostRepeatedNuc;
    }

    public static void recompNuc(String DNASeq) {
        double seqLength = DNASeq.length();
        double aOcurrencies = countA(DNASeq);
        double cOcurrencies = countC(DNASeq);
        double tOcurrencies = countT(DNASeq);
        double gOcurrencies = countG(DNASeq);
        System.out.println("A ocurrencies: " + (int) aOcurrencies + " / Percentage: " + round(aOcurrencies / seqLength * 100) + "%\n"
                + "C ocurrencies: " + (int) cOcurrencies + " / Percentage: " + round(cOcurrencies / seqLength * 100) + "%\n"
                + "T ocurrencies: " + (int) tOcurrencies + " / Percentage: " + round(tOcurrencies / seqLength * 100) + "%\n"
                + "G ocurrencies: " + (int) gOcurrencies + " / Percentage: " + round(gOcurrencies / seqLength * 100) + "%\n"
                + "Total nucleotides of sequence: " + (int) seqLength);
    }

    public static int countA(String DNASeq) {
        int countA = 0;
        for (int i = 0; i < DNASeq.length(); i++) {
            if (DNASeq.charAt(i) == 'A') {
                countA++;
            }
        }
        return countA;
    }

    public static void dnaToRna(String DNASeq) {
        String RNASeq = "";
        for (int i = 0; i < DNASeq.length(); i++) {
            if (DNASeq.charAt(i) == 'T') {
                RNASeq += 'U';
            } else {
                RNASeq += DNASeq.charAt(i);
            }
        }
        System.out.println("The original sequence: " + DNASeq + "\n"
                + "The RNA sequence: " + RNASeq);
    }

    public static void repeatedCodons(String DNASeq) {
        String moreRepeatedCodon = "";
        int timesRepeated = 0;
        HashMap<String, Integer> codons = new HashMap<String, Integer>();
        for (int i = 0; i < DNASeq.length(); i += 3) {
            if(codons.containsKey(DNASeq.substring(i, i + 3))) {
                codons.put(DNASeq.substring(i, i + 3), codons.get(DNASeq.substring(i, i + 3)) + 1);
            } else {
                codons.put(DNASeq.substring(i, i + 3), 1);
            }
        }

        for (String i : codons.keySet()) {
            if(codons.get(i) > timesRepeated) {
                moreRepeatedCodon = i;
                timesRepeated = codons.get(i);
            }
        }
        System.out.println("The most repeated codon is: " + moreRepeatedCodon + " / Times repeated: " + timesRepeated);
    }

    public static int countC(String DNASeq) {
        int countC = 0;
        for (int i = 0; i < DNASeq.length(); i++) {
            if (DNASeq.charAt(i) == 'C') {
                countC++;
            }
        }
        return countC;
    }

    public static int countT(String DNASeq) {
        int countT = 0;
        for (int i = 0; i < DNASeq.length(); i++) {
            if (DNASeq.charAt(i) == 'T') {
                countT++;
            }
        }
        return countT;
    }

    public static int countG(String DNASeq) {
        int countG = 0;
        for (int i = 0; i < DNASeq.length(); i++) {
            if (DNASeq.charAt(i) == 'G') {
                countG++;
            }
        }
        return countG;
    }
}
