/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.plantillam15pt22controlversionsadn.libraries;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gerardgg
 */
public class ADNFileReaderTest {

    /**
     * Test of llegeixFitxerADN method, of class ADNFileReader.
     */
    @Test
    public void testLlegeixFitxerADN() {
        System.out.println("llegeixFitxerADN");
        String nomFitxer = "dnaSequence.txt";
        String expResult = "TTGCGGAAGACTTAACACGATTAA";
        String result = ADNFileReader.llegeixFitxerADN(nomFitxer);
        assertEquals(expResult, result);
    }

    /**
     * Test of validateDNASeq method, of class ADNFileReader.
     */
    @Test
    public void testValidateDNASeq() {
        System.out.println("validateDNASeq");
        String DNASeq = "ACTG";
        boolean expResult = false;
        boolean result = ADNFileReader.validateDNASeq(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of invertDNA method, of class ADNFileReader.
     */
    @Test
    public void testInvertDNA() {
        System.out.println("invertDNA");
        String DNASeq = "ACTG";
        String expResult = "GTCA";
        String result = ADNFileReader.invertDNA(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of findLeastRepeatedNucleotide method, of class ADNFileReader.
     */
    @Test
    public void testFindLeastRepeatedNucleotide() {
        System.out.println("findLeastRepeatedNucleotide");
        String DNASeq = "AAAACCCTTG";
        char expResult = 'G';
        char result = ADNFileReader.findLeastRepeatedNucleotide(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of findMostRepeatedNucleotide method, of class ADNFileReader.
     */
    @Test
    public void testFindMostRepeatedNucleotide() {
        System.out.println("findMostRepeatedNucleotide");
        String DNASeq = "AAAACCCTTG";
        char expResult = 'A';
        char result = ADNFileReader.findMostRepeatedNucleotide(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of recompNuc method, of class ADNFileReader.
     */
    @Test
    public void testRecompNuc() {
        System.out.println("recompNuc");
        String DNASeq = "ACTG";
        ADNFileReader.recompNuc(DNASeq);
    }

    /**
     * Test of countA method, of class ADNFileReader.
     */
    @Test
    public void testCountA() {
        System.out.println("countA");
        String DNASeq = "AAAACCCTTG";
        int expResult = 4;
        int result = ADNFileReader.countA(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of dnaToRna method, of class ADNFileReader.
     */
    @Test
    public void testDnaToRna() {
        System.out.println("dnaToRna");
        String DNASeq = "AAAACCCTTG";
        ADNFileReader.dnaToRna(DNASeq);
    }

    /**
     * Test of repeatedCodons method, of class ADNFileReader.
     */
    @Test
    public void testRepeatedCodons() {
        System.out.println("repeatedCodons");
        String DNASeq = "ACTTGAACT";
        ADNFileReader.repeatedCodons(DNASeq);
    }

    /**
     * Test of countC method, of class ADNFileReader.
     */
    @Test
    public void testCountC() {
        System.out.println("countC");
        String DNASeq = "AAAACCCTTG";
        int expResult = 3;
        int result = ADNFileReader.countC(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of countT method, of class ADNFileReader.
     */
    @Test
    public void testCountT() {
        System.out.println("countT");
        String DNASeq = "AAAACCCTTG";
        int expResult = 2;
        int result = ADNFileReader.countT(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of countG method, of class ADNFileReader.
     */
    @Test
    public void testCountG() {
        System.out.println("countG");
        String DNASeq = "AAAACCCTTG";
        int expResult = 1;
        int result = ADNFileReader.countG(DNASeq);
        assertEquals(expResult, result);
    }
    
    // INCORRECTES
        /**
     * Test of llegeixFitxerADN method, of class ADNFileReader.
     */
    @Test
    public void testLlegeixFitxerADNInc() {
        System.out.println("llegeixFitxerADN");
        String nomFitxer = "dnaSeequence.txt";
        String expResult = "TTGCGGAAGACTTAACACGATTAA";
        String result = ADNFileReader.llegeixFitxerADN(nomFitxer);
        assertEquals(expResult, result);
    }

    /**
     * Test of validateDNASeq method, of class ADNFileReader.
     */
    @Test
    public void testValidateDNASeqInc() {
        System.out.println("validateDNASeq");
        String DNASeq = "ACTGZ";
        boolean expResult = false;
        boolean result = ADNFileReader.validateDNASeq(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of invertDNA method, of class ADNFileReader.
     */
    @Test
    public void testInvertDNAInc() {
        System.out.println("invertDNA");
        String DNASeq = "ACTG";
        String expResult = "GTCG";
        String result = ADNFileReader.invertDNA(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of findLeastRepeatedNucleotide method, of class ADNFileReader.
     */
    @Test
    public void testFindLeastRepeatedNucleotideInc() {
        System.out.println("findLeastRepeatedNucleotide");
        String DNASeq = "AAAACCCTTG";
        char expResult = 'A';
        char result = ADNFileReader.findLeastRepeatedNucleotide(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of findMostRepeatedNucleotide method, of class ADNFileReader.
     */
    @Test
    public void testFindMostRepeatedNucleotideInc() {
        System.out.println("findMostRepeatedNucleotide");
        String DNASeq = "AAAACCCTTG";
        char expResult = 'G';
        char result = ADNFileReader.findMostRepeatedNucleotide(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of countA method, of class ADNFileReader.
     */
    @Test
    public void testCountAInc() {
        System.out.println("countA");
        String DNASeq = "AAAACCCTTG";
        int expResult = 99;
        int result = ADNFileReader.countA(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of countC method, of class ADNFileReader.
     */
    @Test
    public void testCountCInc() {
        System.out.println("countC");
        String DNASeq = "AAAACCCTTG";
        int expResult = 7;
        int result = ADNFileReader.countC(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of countT method, of class ADNFileReader.
     */
    @Test
    public void testCountTInc() {
        System.out.println("countT");
        String DNASeq = "AAAACCCTTG";
        int expResult = 275;
        int result = ADNFileReader.countT(DNASeq);
        assertEquals(expResult, result);
    }

    /**
     * Test of countG method, of class ADNFileReader.
     */
    @Test
    public void testCountGInc() {
        System.out.println("countG");
        String DNASeq = "AAAACCCTTG";
        int expResult = -4;
        int result = ADNFileReader.countG(DNASeq);
        assertEquals(expResult, result);
    }
    
}
